package ru.t1.azarin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    List<User> findAll();

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findOneById(@Nullable String id);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

}
