package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.azarin.tm.comparator.CreatedComparator;
import ru.t1.azarin.tm.comparator.NameComparator;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    protected String getSortColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else return "status";
    }

}
