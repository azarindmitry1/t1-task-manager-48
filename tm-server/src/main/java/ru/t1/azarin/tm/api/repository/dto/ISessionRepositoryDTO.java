package ru.t1.azarin.tm.api.repository.dto;

import ru.t1.azarin.tm.dto.model.SessionDTO;

public interface ISessionRepositoryDTO extends IUserOwnedRepositoryDTO<SessionDTO> {
}
