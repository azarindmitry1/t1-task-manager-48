package ru.t1.azarin.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;

public final class DataXmlSaveJaxbRequest extends AbstractUserRequest {

    public DataXmlSaveJaxbRequest(@Nullable final String token) {
        super(token);
    }

}
